# Marking Validation Tool

## About

Author: Leen Remmelzwaal and Rowan Hoch

Written: September 2019

License: CC BY NC

## Description

A tool for validating tutor grades for the APC examination.

## Environment Setup

1) Install Python 3.6.4 or later: https://www.python.org/ftp/python/3.6.4/python-3.6.4.exe) (25Mb)

2) Install Miniconda (this is to make use of highly optimized C++ implementation of NN training): https://docs.conda.io/en/latest/miniconda.html

3) Open Miniconda command prompt and install dependencies:

```
conda install numpy scipy mkl-service libpython m2w64-toolchain
```

4) Install: Microsoft Build Tools for Visual Studio

* Download (https://visualstudio.microsoft.com/thank-you-downloading-visual-studio/?sku=BuildTools&rel=16)
* Select: Workloads → C++ build tools
* Install options: select only the “Windows 10 SDK”

5) Run: `venv_setup.bat` to setup Virtual Env with PIP dependencies

## Running the code

1) Open a Miniconda command prompt (only required for running optimized NN training, otherwise can use normal CMD).
2) Navigate to the directory containing main.py
3) Run the following in the Windows command line:

```
venv_activate.bat
cls && python main.py
venv_deactivate.bat
```
