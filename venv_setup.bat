cls

:: =================================================
:: Deactivate current Virtual Environment
:: =================================================

@echo off

set VIRTUAL_ENV=

REM Don't use () to avoid problems with them in %PATH%
if not defined _OLD_VIRTUAL_PROMPT goto ENDIFVPROMPT
    set "PROMPT=%_OLD_VIRTUAL_PROMPT%"
    set _OLD_VIRTUAL_PROMPT=
:ENDIFVPROMPT

if not defined _OLD_VIRTUAL_PYTHONHOME goto ENDIFVHOME
    set "PYTHONHOME=%_OLD_VIRTUAL_PYTHONHOME%"
    set _OLD_VIRTUAL_PYTHONHOME=
:ENDIFVHOME

if not defined _OLD_VIRTUAL_PATH goto ENDIFVPATH
    set "PATH=%_OLD_VIRTUAL_PATH%"
    set _OLD_VIRTUAL_PATH=
:ENDIFVPATH

@echo on

:: =================================================
:: Remove old Virtual Environment
:: =================================================

rmdir /Q/S venv

:: =================================================
:: Create new Virtual Environment
:: =================================================

mkdir venv
cd venv
pip install virtualenv
virtualenv ctnn

:: =================================================
:: Activate new Virtual Environment
:: =================================================

@echo off

set "VIRTUAL_ENV=C:\Users\leenr\DOCUME~1\PhD\SOFTWA~1\CTNN_P~1\venv\ctnn"

if defined _OLD_VIRTUAL_PROMPT (
    set "PROMPT=%_OLD_VIRTUAL_PROMPT%"
) else (
    if not defined PROMPT (
        set "PROMPT=$P$G"
    )
    if not defined VIRTUAL_ENV_DISABLE_PROMPT (
        set "_OLD_VIRTUAL_PROMPT=%PROMPT%"
    )
)
if not defined VIRTUAL_ENV_DISABLE_PROMPT (
    set "PROMPT=(ctnn) %PROMPT%"
)

REM Don't use () to avoid problems with them in %PATH%
if defined _OLD_VIRTUAL_PYTHONHOME goto ENDIFVHOME
    set "_OLD_VIRTUAL_PYTHONHOME=%PYTHONHOME%"
:ENDIFVHOME

set PYTHONHOME=

REM if defined _OLD_VIRTUAL_PATH (
if not defined _OLD_VIRTUAL_PATH goto ENDIFVPATH1
    set "PATH=%_OLD_VIRTUAL_PATH%"
:ENDIFVPATH1
REM ) else (
if defined _OLD_VIRTUAL_PATH goto ENDIFVPATH2
    set "_OLD_VIRTUAL_PATH=%PATH%"
:ENDIFVPATH2

set "PATH=%VIRTUAL_ENV%\Scripts;%PATH%"

@echo on

:: =================================================
:: Install Python Packages and Dependencies
:: =================================================

python -m pip install --upgrade pip
pip install theano
pip install keras
pip install numpy
pip install matplotlib
pip install pydot
pip install graphviz
pip install cython
pip install tsne
pip install opencv-python
pip install memory_profiler
pip install psutil
pip install https://github.com/intxcc/pyaudio_portaudio/releases/download/1.1.1/PyAudio-0.2.11-cp37-cp37m-win_amd64.whl
pip install audiogen
pip install wave
pip install tones
pip install playsound
cd ../

set "KERAS_BACKEND=theano"

pip freeze

:: =================================================
