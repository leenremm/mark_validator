# https://victorzhou.com/blog/keras-neural-network-tutorial/

import os
os.environ["KERAS_BACKEND"] = "theano"
import numpy as np
np.random.seed(0)
from keras.models import Sequential, load_model
from keras.layers import Dense
from keras.utils import to_categorical
from keras.datasets import mnist

# ============================================

question = 8 # Values: 1-7 for parts. select 8 for summary

# ============================================

save_dir = os.path.join(os.getcwd(), 'saved_models')
model_name = 'keras_apc_q%s_trained_model.h5' % question

# Initialize dataset
import dataset_001
dataset = dataset_001.init()

# All Inputs and Outputs
inputs = np.array([list(record[0::2][question-1]) for record in dataset], "int")
labels = np.array([list(record[1::2][question-1]) for record in dataset], "int")

# Split into training and testing sets
train_inputs = inputs[:6]
train_labels = labels[:6]
test_inputs = inputs[7:]
test_labels = labels[7:]

# ===========================================================================
# Train / Save Method
# ===========================================================================

method = "save" # Options: save / load

if (method == "save"):

    # Build the model.
    model = Sequential([
      Dense(48, activation='relu', input_shape=(np.prod(train_inputs.shape[1:]),)),
      Dense(48, activation='relu'),
      Dense(48, activation='relu'),
      Dense(48, activation='relu'),
      Dense(np.prod(train_labels.shape[1:]), activation='softmax'),
    ])

    # Compile the model.
    model.compile(
      optimizer='adam',
      loss='mean_squared_error',
      metrics=['accuracy'],
    )

    # Train the model.
    model.fit(
      train_inputs,
      train_labels,
      epochs=100,
      batch_size=1,
    )

    _, accuracy = model.evaluate(train_inputs, train_labels)
    print('Accuracy: %.2f' % (accuracy*100))

    # Save model and weights
    if not os.path.isdir(save_dir):
        os.makedirs(save_dir)
    model_path = os.path.join(save_dir, model_name)
    model.save(model_path)
    print('Model Saved: %s' % model_path)

else:

    # ===========================================================================
    # Load Method
    # ===========================================================================

    model_path = os.path.join(save_dir, model_name)
    model = load_model(model_path)
    print('Model Loaded: %s ' % model_path)

# ===========================================================================
# Testing
# ===========================================================================

for i in range(0,len(test_inputs)):
    value = model.predict_classes(test_inputs[i:i+1])[0]
    print('Prediction (testing image #%d): %s' % (i, value+1))

import pdb; pdb.set_trace()
